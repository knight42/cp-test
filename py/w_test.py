def func(x):
    if x > 2:
        return x + 1
    elif x < -1:
        return -1
    return x


def test_answer():
    assert func(2) == 5


def test_answer2():
    assert func(3) == 4


def test_answer3():
    assert func(-2) == -1
